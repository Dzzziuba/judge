[consequence][]Create journalist user with data from {node}=ch.bger.jahia.rules.Journalist.createUser({node});
[consequence][]Modify user {node}=ch.bger.jahia.rules.Journalist.modifyUser({node});
[consequence][]Delete user {node}=ch.bger.jahia.rules.Journalist.removeUser({node});

