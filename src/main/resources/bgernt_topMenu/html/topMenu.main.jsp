<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<c:set var="menuItemMyParts" value="${jcr:getNodes(currentNode, 'bgernt:menuItemMyPart')}"/>

<c:if test="${renderContext.editMode and empty currentNode.nodes}">
    Main menu to contribute
</c:if>

<div class="mod modNavMain">
    <div class="inner">
        <template:module path="*" nodeTypes="bgernt:menuItemMyPart"/>
        <ul>
            <c:forEach items="${menuItemMyParts}" var="node">
                <c:set var="class_first" value="" />
                <c:if test="${status.first}">
                    <c:set var="class_first">item_first</c:set>
                </c:if>
                <li class="item ${class_first}">
                    <template:module node="${node}"/>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>