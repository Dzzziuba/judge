<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<c:set var="menuItemMyParts" value="${jcr:getNodes(currentNode, 'bgernt:menuItemMyPart')}"/>
<c:set var="homePage" value="${renderContext.site.home}"/>

<div class="mod modNavSub">
    <div class="inner">
        <ul>
            <c:forEach items="${homePage.nodes}" var="e">
                <c:set var="classActive" value=""/>
                <c:if test="${fn:startsWith(renderContext.mainResource.node.path, e.path)}">
                    <c:set var="classActive" value="active"/>
                </c:if>
                <c:if test="${e.primaryNodeType.name eq 'jnt:page'}">
                    <%--and e.properties['j:published'].boolean--%>
                    <li class="item">
                        <a class="l1 ${classActive}" href="${e.url}">
                            <span>${e.properties["jcr:title"].string}</span>
                        </a>
                    </li>
                    <c:if test="${not empty e.nodes}">
                        <ul class="sub">
                            <c:forEach items="${e.nodes}" var="i">
                                <c:set var="classActive" value=""/>
                                <c:if test="${i.primaryNodeType.name eq 'jnt:page'}">
                                    <c:if test="${renderContext.mainResource.node.path eq i.path}">
                                        <c:set var="classActive" value="active"/>
                                    </c:if>
                                    <li>
                                        <a class="l2 ${classActive}" href="${i.url}">
                                            <span>${i.properties["jcr:title"].string}</span>
                                        </a>
                                    </li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </c:if>
                </c:if>
            </c:forEach>
        </ul>
    </div>
</div>