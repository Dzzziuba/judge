<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<form method="post" name="loginForm" action="/cms/login">
    <input type="hidden" name="site" value="">
    <input type="hidden" name="failureRedirect" value="${url.base}${url.resourcePath}">
    
    <c:if test="${not empty param['loginError']}">
        <h1 style="font-family: Arial, sans-serif;font-size: 2.154em;line-height: 1.2;">Login Failed</h1>
        <br>
        <div>
            <p>Authentifizierung fehlgeschlagen. Bitte &#252;berpr&#252;fen Sie den Benutzernamen und das Passwort.</p>
            <p>Authentification &#233;chou&#233;e. Veuillez v&#233;rifier le nom d'utilisateur et le mot de passe.</p>
            <p>Autenticazione fallita. La ringraziamo di verificare il nome dell'utente ed il password.</p>
        </div>
    </c:if>
    <c:if test="${not empty param['authorize']}">
        <h1 style="font-family: Arial, sans-serif;font-size: 2.154em;line-height: 1.2;">You have to be logged in</h1>
        <br>
    </c:if>
    <c:if test="${not empty param['forbidden']}">
        <h1 style="font-family: Arial, sans-serif;font-size: 2.154em;line-height: 1.2;">Forbidden page for your role</h1>
        <br>
    </c:if>
    <c:choose>
        <c:when test="${not empty param.redirectTo}">
            <c:set var="redirectTo" value="${param.redirectTo}"/>
        </c:when>
        <c:otherwise>
            <c:set var="redirectTo" value="${param.service}"/>
        </c:otherwise>
    </c:choose>
    <input type="hidden" name="redirect" value="${fn:escapeXml(redirectTo)}"/>
    
    <div style="margin-top: 30px">
        <label style="margin-top: 10px" for="username"><fmt:message key="bgernt_login"/> </label>
        <div>
            <input type="text" style="margin-top: 10px; width: 300px; height: 25px" id="username" name="username">
        </div>
    </div>
    <div style="margin-top: 30px">
        <label style="margin-top: 10px" for="password"><fmt:message key="bgernt_password"/></label>
        <div>
            <input type="password" style="margin-top: 10px; width: 300px; height: 25px" id="password" name="password">
        </div>
    </div>
    <div style="margin-top: 30px">
        <input type="submit" value="<fmt:message key="bgernt_submit"/>"/>
    </div>
    <div style="margin-top: 30px">
        <a href="#" style="color: darkorange"><fmt:message key="bgernt_forgot_password"/></a>
    </div>
</form>