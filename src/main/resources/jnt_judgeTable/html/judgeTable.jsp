<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="query" uri="http://www.jahia.org/tags/queryLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="s" uri="http://www.jahia.org/tags/search" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<style>
    table {
        width:100%;
    }
    table, th, td {
        border-bottom: black 1px solid;
    }
    th, td {
        padding: 5px;
        text-align: left;
    }
    table#t01 tr:nth-child(even) {
        background-color: #eee;
    }
    table#t01 tr:nth-child(odd) {
        background-color:#fff;
    }
    table#t01 th {
        background-color: black;
        color: white;
    }

</style>
    <c:if test="${param.sort == null or param.order == null}">
        <jcr:sql var="result" sql="select * from [jnt:judge]"/>
    </c:if>
    <c:if test="${param.sort != null and param.order != null}">
        <jcr:sql var="result" sql="select * from [jnt:judge] as judge order by judge.['${param.sort}'] ${param.order}"/>
    </c:if>
    <p style="font-size: 35px;">List of Former Federal Judges</p>
    <table>
        <tr style="border-top: 1px black solid;">
            <th>Name</th>
            <th>Surname</th>
            <th>Start career</th>
            <th>Resignation</th>
            <th>Court</th>
            <th>Canton</th>
            <th>Consignment</th>
            <th>Years</th>
        </tr>
        <tr>
            <td>
                <a href="${contentNode.url}?sort=Name&order=asc"><img src="${url.currentModule}/images/asc.gif"></a>
                <a href="${contentNode.url}?sort=Name&order=desc"><img src="${url.currentModule}/images/desc.gif"></a>
            </td>
            <td></td>
            <td>
                <a href="${contentNode.url}?sort=Start&order=asc"><img src="${url.currentModule}/images/asc.gif"></a>
                <a href="${contentNode.url}?sort=Start&order=desc"><img src="${url.currentModule}/images/desc.gif"></a>
            </td>
            <td></td>
            <td></td>
            <td>
                <a href="${contentNode.url}?sort=Canton&order=asc"><img src="${url.currentModule}/images/asc.gif"></a>
                <a href="${contentNode.url}?sort=Canton&order=desc"><img src="${url.currentModule}/images/desc.gif"></a>
            </td>
            <td></td>
            <td></td>
        </tr>
        <c:forEach items="${result.nodes}" var="judge">
            <tr>
                <td><a onclick="window.open('${contentNode.url}test-table/judge-info.html?id=${judge.identifier}',
                        'newwindow', 'width=500, height=675'); return false;" title="${judge.properties.Name.string}"
                       href="#">${judge.properties.Name.string}</a>
                </td>
                <td>${judge.properties.Surname.string}</td>
                <c:choose>
                    <c:when test="${not empty judge.properties.Start.string}">
                        <td>${judge.properties.Start.string}</td>
                    </c:when>
                    <c:otherwise>
                        <td>-</td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${not empty judge.properties.Resignation.string}">
                        <td>${judge.properties.Resignation.string}</td>
                    </c:when>
                    <c:otherwise>
                        <td>-</td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${not empty judge.properties.Court.string}">
                        <td>${judge.properties.Court.string}</td>
                    </c:when>
                    <c:otherwise>
                        <td>-</td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${not empty judge.properties.Canton.string}">
                        <td>${judge.properties.Canton.string}</td>
                    </c:when>
                    <c:otherwise>
                        <td>-</td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${not empty judge.properties.Consignment.string}">
                        <td>${judge.properties.Consignment.string}</td>
                    </c:when>
                    <c:otherwise>
                        <td>-</td>
                    </c:otherwise>
                </c:choose>
                <td>${judge.properties.Years_birth.string} - ${judge.properties.Years_death.string}</td>
            </tr>
        </c:forEach>
    </table>
    <form method="get" action="<c:url value='${url.base}${renderContext.mainResource.node.path}.ReverseName.do'/>">
        <input type="submit" value="reverse">
    </form>