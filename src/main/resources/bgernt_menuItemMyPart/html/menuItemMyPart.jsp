<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="Title" value="${currentNode.properties['Title'].string}"/>
<c:set var="Reference" value="${currentNode.properties['Reference'].string}"/>

<a href="${url.base}${Reference}">${Title}</a>