<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="query" uri="http://www.jahia.org/tags/queryLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="s" uri="http://www.jahia.org/tags/search" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<c:forEach items="${renderContext.user.properties}" var="property">
    <c:if test="${property.key == 'Title'}"><c:set var="title" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Academic_title'}"><c:set var="academicTitle" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Address'}"><c:set var="address" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'NPA'}"><c:set var="npa" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Place'}"><c:set var="place" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Name'}"><c:set var="name" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Surname'}"><c:set var="surname" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Phone'}"><c:set var="phone" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Cellphone'}"><c:set var="cellphone" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Email'}"><c:set var="email" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Email_additional'}"><c:set var="emailAdditional" value="${fn:substring(property.value, 1, fn:length(property.value)-1)}"/></c:if>
    <c:if test="${property.key == 'Newspapers'}"><c:set var="newspapers" value="${property.value}"/></c:if>
    <c:if test="${property.key == 'Languages'}"><c:set var="languages" value="${fn:substring(property.value, 1, fn:length(property.value)-1)}"/></c:if>
</c:forEach>



<div>
    <div style="width: 600px">
        <h2>Edition de mon profil</h2>
        <h3>Mot de passe</h3>
        <form class="form-horizontal" method="post" action="<c:url value='${url.base}${renderContext.mainResource.node.path}.JournalistForm.do'/>">
            <div class="form-group">
                <label class="control-label col-sm-6" for="password">Mot de passe existant</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="password" name="password">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="passwordNew">Nouveau mot de passe</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="passwordNew" name="passwordNew">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="confirmPasswordNew">Verifier votre nouveau mot de passe</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="confirmPasswordNew" name="confirmPasswordNew">
                </div>
            </div>
            <h3>Mot profil d`accreditation</h3>
            <div class="form-group">
                <label class="control-label col-sm-6" for="journals">Journaux concernes</label>
                <div class="col-sm-6" id="journals">

                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6" for="languages">Langue de travil*</label>
                <div class="col-sm-6" id="languages">
                        <c:if test="${fn:length(languages) > 0}">
                            <div class="checkbox">
                                <c:if test="${fn:contains(languages, 'Allemand')}">
                                    <input type="checkbox" value="Allemand" id="laguage" name="language" checked>Allemand</label>
                                </c:if>
                                <c:if test="${!(fn:contains(languages, 'Allemand'))}">
                                    <input type="checkbox" value="Allemand" id="laguage" name="language">Allemand</label>
                                </c:if>
                            </div>
                            <div class="checkbox">
                                <c:if test="${fn:contains(languages, 'Francais')}">
                                    <input type="checkbox" value="Francais" id="laguage" name="language" checked>Francais</label>
                                </c:if>
                                <c:if test="${!(fn:contains(languages, 'Francais'))}">
                                    <input type="checkbox" value="Francais" id="laguage" name="language">Francais</label>
                                </c:if>
                            </div>
                            <div class="checkbox">
                                <c:if test="${fn:contains(languages, 'Italien')}">
                                    <input type="checkbox" value="Italien" id="laguage" name="language" checked>Italien</label>
                                </c:if>
                                <c:if test="${!(fn:contains(languages, 'Italien'))}">
                                    <input type="checkbox" value="Italien" id="laguage" name="language">Italien</label>
                                </c:if>
                            </div>
                        </c:if>
                        <c:if test="${!(fn:length(languages) > 0)}">
                            <div class="checkbox">
                                <input type="checkbox" value="Allemand" id="laguage" name="language">Allemand</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" value="Francais" id="laguage" name="language">Francais</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" value="Italien" id="laguage" name="language">Italien</label>
                            </div>
                        </c:if>
                    </div>
                </div>
            <h3>Vos coordonnees</h3>

            <div class="form-group">
                <label class="control-label col-sm-6" for="title">Titre</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="title" name="title" value="${title}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="academicTitle">Titre academique</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="academicTitle" name="academicTitle" value="${academicTitle}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="name">Prenom</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="name" name="name" value="${name}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="surname">Nom</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="surname" name="surname" value="${surname}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="address">Adresse</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="address" name="address" value="${address}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="npa">NPA</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="npa" name="npa" value="${npa}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="place">Lieu</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="place" name="place" value="${place}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="phone">No telephone</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="phone" name="phone" value="${phone}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="cellphone">No telephone portable</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="cellphone" name="cellphone" value="${cellphone}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6" for="email">Email</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="email" name="email" value="${email}">
                </div>
            </div>

            <div class="form-group">

                <c:if test="${fn:length(emailAdditional) > 0}">
                    <c:forEach items="${emailAdditional}" var="emailAdd">
                        <label class="control-label col-sm-6" for="emailAdd"></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="emailAdd" name="emailAdd" value="${emailAdd}">
                        </div>
                    </c:forEach>
                    <label class="control-label col-sm-6" for="emailAdd"></label>
                    <div class="col-sm-6" id="emailAdd">
                        <input type="text" class="form-control" name="emailAdd">
                    </div>
                    <label class="control-label col-sm-6" for="emailAdd"></label>
                    <div class="col-sm-6" id="emailAdd">
                        <input type="text" class="form-control" name="emailAdd">
                    </div>
                    <label class="control-label col-sm-6" for="emailAdd"></label>
                    <div class="col-sm-6" id="emailAdd">
                        <input type="text" class="form-control" name="emailAdd">
                    </div>
                </c:if>
                <c:if test="${!(fn:length(emailAdditional) > 0)}">
                    <label class="control-label col-sm-6" for="emailAdd"></label>
                    <div class="col-sm-6" id="emailAdd">
                        <input type="text" class="form-control" name="emailAdd">
                    </div>
                    <label class="control-label col-sm-6" for="emailAdd"></label>
                    <div class="col-sm-6" id="emailAdd">
                        <input type="text" class="form-control" name="emailAdd">
                    </div>
                    <label class="control-label col-sm-6" for="emailAdd"></label>
                    <div class="col-sm-6" id="emailAdd">
                        <input type="text" class="form-control" name="emailAdd">
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Envoyer</button>
                </div>
            </div>
        </form>
    </div>
</div>