<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>
    .hide {
        display: none;
    }
     table, th, td {
         border: 1px solid black;
     }

    th, td {
        padding: 15px;
        text-align: left;
    }
    input {
        margin: 10px;
    }
</style>
<div class="bodywrapper">
    <input type="text" name="searchString" id="searchString" placeholder="search"/>
    <input type="button" onclick="check();" value="press">
    <div id="result">
        <table id="resultTable" class="hide">
            <tr>
                <th><fmt:message key="bgernt_journalist.Name"/></th>
                <th><fmt:message key="bgernt_journalist.Surname"/></th>
                <th><fmt:message key="bgernt_journalist.Title"/></th>
                <th><fmt:message key="bgernt_journalist.Languages"/></th>
                <th><fmt:message key="bgernt_journalist.Place"/></th>
                <th><fmt:message key="bgernt_journalist.NPA"/></th>
                <th><fmt:message key="bgernt_journalist.Email"/></th>
                <th><fmt:message key="bgernt_journalist.Phone"/></th>
                <th><fmt:message key="bgernt_journalist.Cellphone"/></th>
            </tr>
        </table>
    </div>
</div>
<script>

    function doGet(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.withCredentials = true;
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var data = JSON.parse(xhr.responseText);
                callback(data);
            }
            if (xhr.readyState === 4 && xhr.status === 403) {
                var locationHeader = xhr.getResponseHeader("Location");
                if (null !== locationHeader) {
                    window.location.href = locationHeader;
                }
            }
            if (xhr.status === 204) {
                callback();
            }
        };
        xhr.send();
    }

    var generateJournalist = function (data) {
        var htmlTable;
        data['journalists'].forEach(function (journalist) {
            htmlTable +=
                "<tr class=\".res\">"
                + "<td>" + journalist['name'] + "</td>"
                + "<td>" + journalist['surname'] + "</td>"
                + "<td>" + journalist['title'] + "</td>"
                + "<td>" + journalist['lang'] + "</td>"
                + "<td>" + journalist['location'] + "</td>"
                + "<td>" + journalist['zip'] + "</td>"
                + "<td>" + journalist['email'] + "<p>" + journalist['emailExtra'] + "</p>" + "</td>"
                + "<td>" + journalist['phone'] + "</td>"
                + "<td>" + journalist['mobile'] + "</td>"
                + "</tr>";
        });

        if (htmlTable !== undefined && htmlTable !== "") {
            $('#resultTable').removeClass("hide");
            $('#resultTable').append(htmlTable);
        }
    };

    function check() {
        $('#resultTable').find("tr:gt(0)").remove();
        var location = '${url.base}${currentNode.path}.JournalistSearch.do';
        doGet(location + '?searchString=' + $('#searchString').val(), generateJournalist);
    }

</script>