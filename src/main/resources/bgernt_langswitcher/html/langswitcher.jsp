<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<ul class="block block_last">

    <c:choose>
        <c:when test="${fn:contains(url.base, '/fr')}">
            <li class="item item_first"><a href="${url.resourcePath}" target="_top"><span
                    lang="de" title="deutsch">D</span></a></li>

            <li class="item"><a href="/it${url.resourcePath}"
                                target="_top"><span lang="it" title="italiano">I</span></a></li>
        </c:when>
        <c:when test="${fn:contains(url.base, '/it')}">
            <li class="item item_first"><a href="${url.resourcePath}" target="_top"><span
                    lang="de" title="deutsch">D</span></a></li>

            <li class="item"><a href="/fr${url.resourcePath}"
                                target="_top"><span lang="fr" title="francais">F</span></a></li>
        </c:when>
        <c:otherwise>
            <li class="item item_first"><a href="/fr${url.resourcePath}" target="_top"><span
                    lang="fr" title="francais">F</span></a></li>
            <li class="item"><a href="/it${url.resourcePath}"
                                target="_top"><span lang="it" title="italiano">I</span></a></li>
        </c:otherwise>
    </c:choose>
</ul>


