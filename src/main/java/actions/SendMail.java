package actions;

import org.jahia.services.content.decorator.JCRUserNode;
import org.jahia.services.mail.MailService;
import org.jahia.services.render.Resource;
import org.slf4j.Logger;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.script.ScriptException;
import java.util.HashMap;
import java.util.Map;

/**
 * Get the root under the homepage for a page.
 * <p>
 * Example :
 * Home
 * - Menu Root 1
 * - Page 11
 * - Page 12
 * - Page 121
 * - Menu Root 2
 * -Page 21
 * - Page 211
 * <p>
 * For node "Page 121", the result is "Menu Root 1"
 * For node "Page 11", the result is "Menu Root 1"
 * For node "Page 21", the result is "Menu Root 2"
 *
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public final class SendMail {
    /**
     * The variable LOGGER
     */
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(SendMail.class);


    /**
     * service for email sending
     */
    private MailService mailService;
    private String fromMail;

    /**
     * Send email to journalist
     *
     * @param userNode     user node which connected with journalist
     * @param templateNode template node for subject header and footer
     * @param resource     contains metadata. Passed from action
     *
     * @throws ScriptException
     * @throws RepositoryException
     */
    public void sendEmail(final JCRUserNode userNode, final Node templateNode, final Resource resource)
            throws ScriptException, RepositoryException {

        LOGGER.debug("sendEmail : Composing message");

        final Map<String, Object> parameters = new HashMap<>();

        parameters.put("title", userNode.getProperty("Title").getString());
        parameters.put("academicTitle", userNode.getProperty("Academic_title").getString());
        parameters.put("name", userNode.getProperty("Name").getString());
        parameters.put("surname", userNode.getProperty("Surname").getString());
        parameters.put("address", userNode.getProperty("Address").getString());
        parameters.put("npa", userNode.getProperty("NPA").getString());
        parameters.put("place", userNode.getProperty("Place").getString());
        parameters.put("phone", userNode.getProperty("Phone").getString());
        parameters.put("cellphone", userNode.getProperty("Cellphone").getString());
        parameters.put("email", userNode.getProperty("Email").getString());
        parameters.put("email_additional", userNode.getPropertyAsString("Email_additional"));
        parameters.put("header", templateNode.getProperty("Header").getString());
        parameters.put("footer", templateNode.getProperty("Footer").getString());
        parameters.put("subject", templateNode.getProperty("Subject").getString());

        mailService.sendMessageWithTemplate("templates/email-template.vm", parameters,
                (String) parameters.get("email"), fromMail,
                "", "", resource.getLocale(), "BGER jahia templates");
        LOGGER.debug("sendEmail : Message was sent successfully");
    }

    public void sendCustomMail(final String email, final String message) {
        mailService.sendMessage(email, message);
    }

    /**
     * Setter for mailService
     *
     * @param mailService
     */
    public void setMailService(final MailService mailService) {
        this.mailService = mailService;
    }

    public void setFromMail(final String fromMail) {
        this.fromMail = fromMail;
    }
}
