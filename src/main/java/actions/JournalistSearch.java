package actions;

import org.apache.commons.lang3.StringUtils;
import org.jahia.bin.Action;
import org.jahia.bin.ActionResult;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.render.RenderContext;
import org.jahia.services.render.Resource;
import org.jahia.services.render.URLResolver;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;

import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Roma Dziuba <rodzi@smile.fr>
 */
public class JournalistSearch extends Action {
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(JournalistSearch.class);
    private static final String LIKE_CLOSE = "%'";

    /**
     * @param req
     * @param renderContext
     * @param resource
     * @param session
     * @param parameters
     * @param urlResolver
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ActionResult doExecute(final HttpServletRequest req, final RenderContext renderContext,
                                  final Resource resource, final JCRSessionWrapper session,
                                  final Map<String, List<String>> parameters, final URLResolver urlResolver)
            throws RepositoryException, JSONException {
        final String searchString = parameters.get("searchString").get(0);
        String name = "";
        String surname = "";
        final String[] words = searchString.split(" ");
        if (words.length > 1) {
            name = words[0];
            surname = words[1];
        } else {
            if (words.length == 1) {
                name = words[0];
            } else {
                return ActionResult.INTERNAL_ERROR;
            }
        }
        final StringBuilder queryString = new StringBuilder(100);
        queryString.append("select * from [bgernt:journalist] as j ");
        if (StringUtils.isNoneBlank(name) && StringUtils.isNoneBlank(surname)) {
            queryString.append("where j.[Name] like '%" + name + "%' or j.[Surname] like '%" + surname + LIKE_CLOSE
                    + "or (j.[Surname] like '%" + surname + "%' and  j.[Name] like '%" + name + LIKE_CLOSE);
        } else {
            if (StringUtils.isNoneBlank(name)) {
                queryString.append("where j.[Name] like '%" + name + "%' or j.[Surname] like '%" + name + LIKE_CLOSE);
            } else {
                if (StringUtils.isNoneBlank(surname)) {
                    queryString.append("where j.[Name] like '%"
                            + surname + "%' or j.[Surname] like '%" + surname + LIKE_CLOSE);
                } else {
                    return ActionResult.INTERNAL_ERROR;
                }
            }
        }
        final QueryManager jcrQueryManager = session.getWorkspace().getQueryManager();
        final Query query = jcrQueryManager.createQuery(queryString.toString(), "JCR-SQL2");
        final NodeIterator nodeIterator = query.execute().getNodes();
        final List<Map<String, String>> result = new ArrayList<>();

        JCRNodeWrapper nextNode;
        while (nodeIterator.hasNext()) {
            nextNode = (JCRNodeWrapper) nodeIterator.nextNode();
            LOGGER.info(nextNode.getPropertyAsString("Name") + " " + nextNode.getPropertyAsString("Surname"));
            final Map<String, String> nodeMap = new HashMap<>();
            if (StringUtils.isNoneBlank(nextNode.getProperty("Name").getString())) {
                nodeMap.put("name", nextNode.getProperty("Name").getString());
            } else {
                nodeMap.put("name", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getProperty("Surname").getString())) {
                nodeMap.put("surname", nextNode.getProperty("Surname").getString());
            } else {
                nodeMap.put("surname", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getProperty("Title").getString())) {
                nodeMap.put("title", nextNode.getProperty("Title").getString());
            } else {
                nodeMap.put("title", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getProperty("Place").getString())) {
                nodeMap.put("location", nextNode.getProperty("Place").getString());
            } else {
                nodeMap.put("location", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getProperty("NPA").getString())) {
                nodeMap.put("zip", nextNode.getProperty("NPA").getString());

            } else {
                nodeMap.put("zip", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getProperty("Email").getString())) {
                nodeMap.put("email", nextNode.getProperty("Email").getString());

            } else {
                nodeMap.put("email", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getPropertyAsString("Email_additional"))) {
                nodeMap.put("emailExtra", nextNode.getPropertyAsString("Email_additional"));

            } else {
                nodeMap.put("emailExtra", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getProperty("Phone").getString())) {
                nodeMap.put("phone", nextNode.getProperty("Phone").getString());

            } else {
                nodeMap.put("phone", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getProperty("Cellphone").getString())) {
                nodeMap.put("mobile", nextNode.getProperty("Cellphone").getString());

            } else {
                nodeMap.put("mobile", "");
            }
            if (StringUtils.isNoneBlank(nextNode.getPropertyAsString("Languages"))) {
                nodeMap.put("lang", nextNode.getPropertyAsString("Languages"));
            } else {
                nodeMap.put("lang", "");
            }

            result.add(nodeMap);
        }
        final ActionResult actRes = ActionResult.OK_JSON;
        final JSONObject json = new JSONObject();
        json.put("journalists", result);
        actRes.setJson(json);
        actRes.setResultCode(HttpServletResponse.SC_OK);
        LOGGER.info(actRes.getJson().toString());
        return actRes;
    }
}
