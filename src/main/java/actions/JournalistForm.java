package actions;

import org.apache.commons.lang.StringUtils;
import org.jahia.api.Constants;
import org.jahia.bin.Action;
import org.jahia.bin.ActionResult;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRPublicationService;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.decorator.JCRUserNode;
import org.jahia.services.render.RenderContext;
import org.jahia.services.render.Resource;
import org.jahia.services.render.URLResolver;
import org.jahia.services.usermanager.JahiaUserManagerService;
import org.slf4j.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * Get the root under the homepage for a page.
 * <p>
 * Example :
 * Home
 * - Menu Root 1
 * - Page 11
 * - Page 12
 * - Page 121
 * - Menu Root 2
 * -Page 21
 * - Page 211
 * <p>
 * For node "Page 121", the result is "Menu Root 1"
 * For node "Page 11", the result is "Menu Root 1"
 * For node "Page 21", the result is "Menu Root 2"
 *
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public class JournalistForm extends Action {
    /**
     * The variable LOGGER
     */
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(JournalistForm.class);
    /**
     * The variable sendMail
     */
    private SendMail sendMail;

    /**
     * Action which execute when you submit journalist form
     *
     * @throws Exception
     */
    @Override
    public ActionResult doExecute(final HttpServletRequest req, final RenderContext renderContext,
                                  final Resource resource,
                                  final JCRSessionWrapper session, final Map<String, List<String>> parameters,
                                  final URLResolver urlResolver) throws java.lang.Exception {
        final int successCode = 200;
        final QueryManager jcrQueryManager = session.getWorkspace().getQueryManager();
        final Query query = jcrQueryManager.createQuery("select * from [bgernt:journalist] as journalist where"
                + " journalist.[j:nodename] = '" + session.getUser().getName() + "'", "JCR-SQL2");
        final NodeIterator nodeIterator = query.execute().getNodes();
        final String password = parameters.get("password").get(0);
        final String newPassword = parameters.get("passwordNew").get(0);
        final String confirmNewPassword = parameters.get("confirmPasswordNew").get(0);
        final String address = parameters.get("address").get(0);
        final String npa = parameters.get("npa").get(0);
        final String place = parameters.get("place").get(0);
        final String phone = parameters.get("phone").get(0);
        final String cellphone = parameters.get("cellphone").get(0);
        final String email = parameters.get("email").get(0);
        final List<String> languages = parameters.get("language");
        final List<String> emailAdd = parameters.get("emailAdd");

        final JCRSessionWrapper editSession = JCRSessionFactory.getInstance()
                .getCurrentUserSession(Constants.EDIT_WORKSPACE);

        final JCRNodeWrapper user = session.getNodeByIdentifier(session.getUser().getProperty("jcr:uuid"));
        final JCRUserNode jcrUserNode = JahiaUserManagerService.getInstance().lookupUser(user.getName());
        while (nodeIterator.hasNext()) {
            final Node node = nodeIterator.nextNode();
            if (node.getProperty("Password").getString().compareTo(password) == 0
                    && newPassword.compareTo(confirmNewPassword) == 0) {
                node.setProperty("Password", newPassword);
                jcrUserNode.setProperty("Password", newPassword);
                jcrUserNode.setPassword(newPassword);

            }

            languages.removeIf(StringUtils::isBlank);
            emailAdd.removeIf(StringUtils::isBlank);
            String[] langArr = new String[languages.size()];
            langArr = languages.toArray(langArr);
            String[] emailArr = new String[emailAdd.size()];
            emailArr = emailAdd.toArray(emailArr);

            node.setProperty("Address", address);
            node.setProperty("NPA", npa);
            node.setProperty("Place", place);
            node.setProperty("Phone", phone);
            node.setProperty("Cellphone", cellphone);
            node.setProperty("Email", email);
            node.setProperty("Email_additional", emailArr);
            node.setProperty("Languages", languages.toArray(new String[languages.size()]));

            jcrUserNode.setProperty("Address", address);
            jcrUserNode.setProperty("NPA", npa);
            jcrUserNode.setProperty("Place", place);
            jcrUserNode.setProperty("Phone", phone);
            jcrUserNode.setProperty("Cellphone", cellphone);
            jcrUserNode.setProperty("Email", email);
            jcrUserNode.setProperty("Languages", Arrays.toString(langArr));
            jcrUserNode.setProperty("Email_additional", Arrays.toString(emailArr));


            user.refresh(true);
            node.refresh(true);
            jcrUserNode.refresh(true);
            jcrUserNode.save();
            editSession.save();
            session.save();
            final List<String> uuids = new ArrayList<>(Arrays.asList(user.getIdentifier(), node.getIdentifier(),
                    jcrUserNode.getIdentifier()));
            JCRPublicationService.getInstance().publish(uuids, Constants.LIVE_WORKSPACE,
                    Constants.EDIT_WORKSPACE,
                    Collections.emptyList());
        }

        try {
            sendMail.sendEmail(jcrUserNode, getAttributesForTemplate(jcrQueryManager), resource);
        } catch (ScriptException | RepositoryException e) {
            LOGGER.debug("doExecute : Message was not sent");
            LOGGER.info(e.toString());
        }
        return new ActionResult(successCode, renderContext.getMainResource().getNode().getPath());
    }


    /**
     * Return node which contains subject, header and footer for email message
     *
     * @param jcrQueryManager
     */
    private Node getAttributesForTemplate(final QueryManager jcrQueryManager) throws RepositoryException {
        LOGGER.debug("getAttributesForTamplate : Find template node");

        final Query templateQuery = jcrQueryManager.createQuery("select * from [bgernt:emailtemplate] as template where"
                + " template.[Template_name] = 'emailEditJournalistTemplate'", "JCR-SQL2");
        final NodeIterator nodeIteratorForTemplate = templateQuery.execute().getNodes();

        if (nodeIteratorForTemplate.hasNext()) {
            LOGGER.debug("getAttributesForTemplate : Iterator has next");
            return nodeIteratorForTemplate.nextNode();
        }
        return null;
    }

    /**
     * Setter for object which send mail
     *
     * @param sendMail
     */
    public void setSendMail(final SendMail sendMail) {
        this.sendMail = sendMail;
    }
}
