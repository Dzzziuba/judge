package filters;

import org.jahia.services.render.RenderContext;
import org.jahia.services.render.Resource;
import org.jahia.services.render.filter.AbstractFilter;
import org.jahia.services.render.filter.RenderChain;
import org.slf4j.Logger;
import util.JahiaUtil;

/**
 * @author Roma Dziuba <rodzi@smile.fr>
 */
public class RedirectFilter extends AbstractFilter {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(RedirectFilter.class);
    private String checkUrl;

    /**
     * @param previousOut
     * @param renderContext
     * @param resource
     * @param chain
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public String execute(final String previousOut, final RenderContext renderContext, final Resource resource,
                          final RenderChain chain) throws java.lang.Exception {
        String uri = renderContext.getRequest().getRequestURL().toString();
        if (uri.contains(checkUrl)) {
            final String debugMessage = "execute redirected from " + uri + " to ";
            uri = uri.replace(JahiaUtil.LIVE_MODE, JahiaUtil.PREVIEW_MODE);
            renderContext.getResponse().sendRedirect(uri);
            LOGGER.debug(debugMessage + uri);
        }
        return super.execute(previousOut, renderContext, resource, chain);
    }

    public void setCheckUrl(final String checkUrl) {
        this.checkUrl = checkUrl;
    }
}
