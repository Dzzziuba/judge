package util;

import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.usermanager.JahiaUserManagerService;

import javax.jcr.RepositoryException;

/**
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public final class JahiaUtil {
    /**
     * Constant for setting edit mode path in url
     */
    public static final String LIVE_MODE = "render/live";
    /**
     * Constant for setting preview mode path in url
     */
    public static final String PREVIEW_MODE = "render/default";
    private JahiaUtil() {

    }

    /**
     * Simple method for getting session for current user
     * @return
     * @throws RepositoryException
     */
    public static JCRSessionWrapper getCurrenUserSession() throws RepositoryException {
        return JCRSessionFactory.getInstance()
                .getCurrentUserSession();
    }

    /**
     * Simple method for getting session for current user from determine workspace
     * @param workspace
     * @return
     * @throws RepositoryException
     */
    public static JCRSessionWrapper getCurrenUserSession(final String workspace) throws RepositoryException {
        return JCRSessionFactory.getInstance()
                .getCurrentUserSession(workspace);
    }

    /**
     * Set root user for current session
     */
    public static void setRootUser() {
        JCRSessionFactory.getInstance().setCurrentUser(JahiaUserManagerService.getInstance().lookupRootUser()
                .getJahiaUser());
    }
}
