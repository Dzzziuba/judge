package ch.bger.jahia.background;

import org.jahia.api.Constants;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.scheduler.BackgroundJob;
import org.jahia.services.usermanager.JahiaUserManagerService;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;

/**
 * Get the root under the homepage for a page.
 * <p>
 * Example :
 * Home
 * - Menu Root 1
 * - Page 11
 * - Page 12
 * - Page 121
 * - Menu Root 2
 * -Page 21
 * - Page 211
 * <p>
 * For node "Page 121", the result is "Menu Root 1"
 * For node "Page 11", the result is "Menu Root 1"
 * For node "Page 21", the result is "Menu Root 2"
 *
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public class CheckDisabledJournalist extends BackgroundJob {
    /**
     * The variable LOGGER
     */
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(CheckDisabledJournalist.class);

    /**
     * hello from the other side
     *
     * @param jobExecutionContext
     * @throws Exception
     */
    @Override
    public void executeJahiaJob(final JobExecutionContext jobExecutionContext) throws java.lang.Exception {
        JCRSessionFactory.getInstance().setCurrentUser(JahiaUserManagerService.getInstance().lookupRootUser()
                .getJahiaUser());
        final JahiaUserManagerService jahiaUserManagerService = JahiaUserManagerService.getInstance();
        final JCRSessionWrapper editSession = JCRSessionFactory.getInstance()
                .getCurrentUserSession(Constants.EDIT_WORKSPACE);
        final JCRSessionWrapper liveSession = JCRSessionFactory.getInstance()
                .getCurrentUserSession(Constants.LIVE_WORKSPACE);
        final QueryManager jcrQueryManager = editSession.getWorkspace().getQueryManager();
        final Query query = jcrQueryManager.createQuery("select * from [bgernt:journalist]", "JCR-SQL2");
        final NodeIterator nodeIterator = query.execute().getNodes();
        while (nodeIterator.hasNext()) {
            final Node currentNode = nodeIterator.nextNode();
            if (currentNode.getProperty("Enable").getBoolean()) {
                LOGGER.info("Journalist " + currentNode.getProperty("Name").getString() + " "
                        + currentNode.getProperty("Surname").getString() + " is enable");
            } else {
                LOGGER.info("Journalist " + currentNode.getProperty("Name").getString()
                        + currentNode.getProperty("Surname").getString() + " is disable");
                final String nodeIdentifier = currentNode.getIdentifier();
                jahiaUserManagerService.deleteUser(jahiaUserManagerService.getUserPath(currentNode.getName()),
                        editSession);
                currentNode.remove();
                editSession.save();
                liveSession.getNodeByIdentifier(nodeIdentifier).remove();
                liveSession.save();
            }
        }
    }
}
