package ch.bger.jahia.background;

import ch.bger.jahia.rules.Journalist;
import org.apache.commons.lang3.StringUtils;
import org.jahia.api.Constants;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.scheduler.BackgroundJob;
import org.jahia.services.usermanager.JahiaUserManagerService;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import java.util.Collections;

/**
 * @author Roma Dziuba <rodzi@smile.fr>
 */
public class CreateJournalistFromUser extends BackgroundJob {
    /**
     * The variable LOGGER
     */
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(CheckDisabledJournalist.class);
    private static final String J_NODENAME = "j:nodename";


    /**
     * hello from the other side
     *
     * @param jobExecutionContext
     *
     * @throws Exception
     */
    @Override
    public void executeJahiaJob(final JobExecutionContext jobExecutionContext) throws java.lang.Exception {
        LOGGER.info("Backgroundjob for creating journalist started");
        execute();
    }

    /**
     * @param user
     * @param site
     * @param session
     *
     * @throws RepositoryException
     */
    private boolean addJournalist(final Node user, final JCRNodeWrapper site, final JCRSessionWrapper session)
            throws RepositoryException {
        final JCRNodeWrapper journalist = site.addNode(user.getProperty(J_NODENAME).getString(), "bgernt:journalist");

        if (StringUtils.isBlank(user.getProperty(J_NODENAME).getString())) {
            return rejectUser(session, journalist, site, J_NODENAME);
        } else {
            journalist.setProperty(J_NODENAME, user.getProperty(J_NODENAME).getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:password").getString())) {
            return rejectUser(session, journalist, site, "j:password");
        } else {
            journalist.setProperty("Password", user.getProperty("j:password").getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:firstName").getString())) {
            return rejectUser(session, journalist, site, "j:firstName");
        } else {
            journalist.setProperty("Name", user.getProperty("j:firstName").getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:lastName").getString())) {
            return rejectUser(session, journalist, site, "j:lastName");
        } else {
            journalist.setProperty("Surname", user.getProperty("j:lastName").getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:email").getString())) {
            return rejectUser(session, journalist, site, "j:email");
        } else {
            journalist.setProperty("Email", user.getProperty("j:email").getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:title").getString())) {
            return rejectUser(session, journalist, site, "j:title");

        } else {
            journalist.setProperty("Title", user.getProperty("j:title").getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:address").getString())) {
            return rejectUser(session, journalist, site, "j:address");

        } else {
            journalist.setProperty("Address", user.getProperty("j:address").getString());
        }

        if (StringUtils.isNumeric(user.getProperty("j:zipCode").getString())) {
            journalist.setProperty("NPA", user.getProperty("j:zipCode").getString());
        } else {
            return rejectUser(session, journalist, site, "j:zipCode");

        }

        if (StringUtils.isBlank(user.getProperty("j:city").getString())) {
            return rejectUser(session, journalist, site, "j:city");

        } else {
            journalist.setProperty("Place", user.getProperty("j:city").getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:phoneNumber").getString())) {
            return rejectUser(session, journalist, site, "j:phoneNumber");

        } else {
            journalist.setProperty("Phone", user.getProperty("j:phoneNumber").getString());
        }

        if (StringUtils.isBlank(user.getProperty("j:mobileNumber").getString())) {
            return rejectUser(session, journalist, site, "j:mobileNumber");

        } else {
            journalist.setProperty("Cellphone", user.getProperty("j:mobileNumber").getString());
        }

        journalist.setProperty("Enable", user.getProperty("j:accountLocked").getBoolean());

        journalist.setProperty("Identity", user.getIdentifier());

        return acceptUser(session, site, journalist);
    }

    /**
     * @throws RepositoryException
     */
    private void execute() throws RepositoryException {
        LOGGER.info("===============MAIN METHOD IN CREATEJOURNALISTFROMUSER started================");
        JCRSessionFactory.getInstance().setCurrentUser(JahiaUserManagerService.getInstance().lookupRootUser()
                .getJahiaUser());

        final JCRSessionWrapper editSession = JCRSessionFactory.getInstance()
                .getCurrentUserSession(Constants.EDIT_WORKSPACE);

        final QueryManager jcrQueryManager = editSession.getWorkspace().getQueryManager();

        final Query query = jcrQueryManager.createQuery("select * from [jnt:user] as u "
                + "where isdescendantnode('/sites/bger2')", "JCR-SQL2");


        final NodeIterator nodeIterator = query.execute().getNodes();

        final JCRNodeWrapper journalistFolder = editSession.getNode("/sites/bger2/contents/journalists");


        while (nodeIterator.hasNext()) {
            final Node user = nodeIterator.nextNode();
            final Query query2 = jcrQueryManager.createQuery("select * from [bgernt:journalist] as u "
                    + "where u.[" + J_NODENAME + "]='" + user.getProperty(J_NODENAME).getString() + "'", "JCR-SQL2");
            if (query2.execute().getNodes().getSize() < 1) {
                LOGGER.info(user.getProperty(J_NODENAME).getString() + " is absent.");
                if (addJournalist(user, journalistFolder, editSession)) {
                    LOGGER.info(user.getProperty(J_NODENAME).getString() + " is created.");
                } else {
                    LOGGER.info(user.getProperty(J_NODENAME).getString() + " is rejected.");
                }
            }
        }
    }

    private boolean rejectUser(final JCRSessionWrapper session, final Node journalist, final JCRNodeWrapper site,
                               final String field) throws RepositoryException {
        LOGGER.info(String.format("user has invalid %s field, rejected.", field));
        journalist.remove();
        site.refresh(false);
        session.save();
        return false;
    }

    private boolean acceptUser(final JCRSessionWrapper session, final Node journalist, final JCRNodeWrapper site)
            throws RepositoryException {

        journalist.refresh(true);
        site.refresh(true);
        session.save();
        Journalist.publish(Collections.singletonList(journalist.getIdentifier()));
        return true;
    }
}
