package ch.bger.jahia.taglib;

import org.jahia.api.Constants;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.usermanager.JahiaUserManagerService;
import org.jahia.taglibs.AbstractJahiaTag;
import org.slf4j.Logger;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.RowIterator;

/**
 * Get the root under the homepage for a page.
 * <p>
 * Example :
 * Home
 * - Menu Root 1
 * - Page 11
 * - Page 12
 * - Page 121
 * - Menu Root 2
 * -Page 21
 * - Page 211
 * <p>
 * For node "Page 121", the result is "Menu Root 1"
 * For node "Page 11", the result is "Menu Root 1"
 * For node "Page 21", the result is "Menu Root 2"
 *
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public class JournalistShowPropertyTag extends AbstractJahiaTag {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(JournalistEditTag.class);
    private String systemName;
    private String propertyName;
    private static final String PROPERTY_VALUE = "propertyValue";

    /**
     * @param systemName
     */
    public void setSystemName(final String systemName) {
        this.systemName = systemName;
    }

    /**
     * @param propertyName
     */
    public void setPropertyName(final String propertyName) {
        this.propertyName = propertyName;
    }


    /**
     * @param systemName2
     * @param propertyName2
     * @throws RepositoryException
     */
    private void getJournalistProperty(final String systemName2, final String propertyName2)
            throws RepositoryException {
        LOGGER.debug("getJournalistProperty execute");
        LOGGER.debug(String.format("getJournalistProperty : sysName == {0}, propName == {1}",
                systemName2, propertyName2));
        JCRSessionFactory.getInstance().setCurrentUser(JahiaUserManagerService.getInstance().lookupRootUser()
                .getJahiaUser());
        final JCRSessionWrapper session = JCRSessionFactory.getInstance()
                .getCurrentUserSession(Constants.EDIT_WORKSPACE);
        final QueryManager jcrQueryManager = session.getWorkspace().getQueryManager();
        final Query query = jcrQueryManager
                .createQuery("select " + propertyName2 + " from [bgernt:journalist] as journalist where"
                        + " journalist.[j:nodename] = '" + systemName2 + "'", "JCR-SQL2");
        final RowIterator rowIterator = query.execute().getRows();
        if (rowIterator.hasNext()) {
            LOGGER.debug("getJournalistProperty iterator has next");
            final Value jour = rowIterator.nextRow().getValue(propertyName2);
            LOGGER.debug("getJournalistProperty : returned value " + jour.getString());
            pageContext.setAttribute(PROPERTY_VALUE, jour.getString());
        }

    }

    /**
     * @return
     */
    @Override
    public int doStartTag() {
        return EVAL_BODY_INCLUDE;
    }

    /**
     * @return
     */
    @Override
    public int doEndTag() {
        try {
            getJournalistProperty(systemName, propertyName);
        } catch (RepositoryException e) {
            LOGGER.info(e.getMessage());
        }
        return EVAL_PAGE;
    }

}
