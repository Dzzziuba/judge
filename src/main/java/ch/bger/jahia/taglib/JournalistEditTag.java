package ch.bger.jahia.taglib;

import org.jahia.api.Constants;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.taglibs.AbstractJahiaTag;
import org.slf4j.Logger;
import util.JahiaUtil;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;

/**
 * Get the root under the homepage for a page.
 * <p>
 * Example :
 * Home
 * - Menu Root 1
 * - Page 11
 * - Page 12
 * - Page 121
 * - Menu Root 2
 * -Page 21
 * - Page 211
 * <p>
 * For node "Page 121", the result is "Menu Root 1"
 * For node "Page 11", the result is "Menu Root 1"
 * For node "Page 21", the result is "Menu Root 2"
 *
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public class JournalistEditTag extends AbstractJahiaTag {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(JournalistEditTag.class);
    private String systemName;
    private String propertyName;
    private String propertyValue;

    /**
     * @param systemName
     */
    public void setSystemName(final String systemName) {
        this.systemName = systemName;
    }

    /**
     * @param propertyName
     */
    public void setPropertyName(final String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @param propertyValue
     */
    public void setPropertyValue(final String propertyValue) {
        this.propertyValue = propertyValue;
    }

    /**
     * @param systemName2
     * @param propertyName2
     * @param propertyValue2
     *
     * @throws RepositoryException
     */
    private void editJournalist(final String systemName2, final String propertyName2, final String propertyValue2)
            throws RepositoryException {
        LOGGER.debug("editJournalist execute");
        LOGGER.debug(String.format("editJournalist : sysName {0}, propName {1}, propValue {2}",
                systemName2, propertyName2, propertyValue2));
        JahiaUtil.setRootUser();
        final JCRSessionWrapper session = JahiaUtil.getCurrenUserSession(Constants.EDIT_WORKSPACE);
        final QueryManager jcrQueryManager = session.getWorkspace().getQueryManager();
        final Query query = jcrQueryManager.createQuery("select * from [bgernt:journalist] as journalist where"
                + " journalist.[j:nodename] = '" + systemName2 + "'", "JCR-SQL2");
        final NodeIterator nodeIterator = query.execute().getNodes();
        if (nodeIterator.hasNext()) {
            LOGGER.debug("editJournalist : Iterator has next");
            final Node jour = nodeIterator.nextNode();
            jour.setProperty(propertyName2, propertyValue2);
            LOGGER.debug(String.format("editJournalist : property {0} == {1}",
                    propertyName2, jour.getProperty(propertyName2).getString()));
            jour.refresh(true);
            session.save();
        }
    }

    /**
     * @return
     */
    @Override
    public int doStartTag() {
        return EVAL_BODY_INCLUDE;
    }

    /**
     * @return
     */
    @Override
    public int doEndTag() {
        try {
            editJournalist(systemName, propertyName, propertyValue);
        } catch (RepositoryException e) {
            LOGGER.info(e.getMessage());
        }
        return EVAL_PAGE;
    }
}
