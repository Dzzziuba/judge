
package ch.bger.jahia.rules;

import org.jahia.api.Constants;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRPublicationService;
import org.jahia.services.content.JCRSessionFactory;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.JCRTemplate;
import org.jahia.services.content.rules.AddedNodeFact;
import org.jahia.services.content.rules.PublishedNodeFact;
import org.jahia.services.usermanager.JahiaUserManagerService;
import org.slf4j.Logger;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.nodetype.NodeType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;


/**
 * Get the root under the homepage for a page.
 * <p>
 * Example :
 * Home
 * - Menu Root 1
 * - Page 11
 * - Page 12
 * - Page 121
 * - Menu Root 2
 * -Page 21
 * - Page 211
 * <p>
 * For node "Page 121", the result is "Menu Root 1"
 * For node "Page 11", the result is "Menu Root 1"
 * For node "Page 21", the result is "Menu Root 2"
 *
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public final class Journalist {

    /**
     * The variable LOGGER
     */
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(Journalist.class);

    private static final String NAME = "Name";
    private static final String SURNAME = "Surname";
    private static final String TITLE = "Title";
    private static final String ACADEMIC_TITLE = "Academic_title";
    private static final String ADDRESS = "Address";
    private static final String NPA = "NPA";
    private static final String PLACE = "Place";
    private static final String PHONE = "Phone";
    private static final String CELLPHONE = "Cellphone";
    private static final String EMAIL = "Email";
    private static final String EMAIL_ADDITIONAL = "Email_additional";
    private static final String NEWSPAPER = "Newspapers";
    private static final String LANGUAGES = "Languages";
    private static final String DEF_VAL = "-";

    /**
     * hello
     */
    public static void createUser(final AddedNodeFact addedNodeFact)
            throws RepositoryException {
        final JCRSessionWrapper currentUserSession = JCRSessionFactory.getInstance()
                .getCurrentSystemSession(Constants.EDIT_WORKSPACE, null, null);
        final Node node = addedNodeFact.getNode();
        final Properties properties = new Properties();
        properties.setProperty(NAME, node.getProperty(NAME).getString());
        properties.setProperty(TITLE, node.getProperty(TITLE).getString());
        if (node.hasProperty(ACADEMIC_TITLE)) {
            properties.setProperty(ACADEMIC_TITLE, node.getProperty(ACADEMIC_TITLE).getString());
        } else {
            properties.setProperty(ACADEMIC_TITLE, DEF_VAL);
        }
        properties.setProperty(SURNAME, node.getProperty(SURNAME).getString());
        properties.setProperty(ADDRESS, node.getProperty(ADDRESS).getString());
        properties.setProperty(NPA, String.valueOf(node.getProperty(NPA).getLong()));
        properties.setProperty(PLACE, node.getProperty(PLACE).getString());
        if (node.hasProperty(PHONE)) {
            properties.setProperty(PHONE, node.getProperty(PHONE).getString());
        } else {
            properties.setProperty(PHONE, DEF_VAL);
        }
        if (node.hasProperty(CELLPHONE)) {
            properties.setProperty(CELLPHONE, node.getProperty(CELLPHONE).getString());
        } else {
            properties.setProperty(CELLPHONE, DEF_VAL);
        }
        properties.setProperty(EMAIL, node.getProperty(EMAIL).getString());
        if (node.hasProperty(NEWSPAPER)) {
            properties.setProperty(NEWSPAPER, node.getProperty(NEWSPAPER).getString());
        } else {
            properties.setProperty(NEWSPAPER, DEF_VAL);
        }
        Value[] values;
        List<String> languages = new ArrayList<>();
        if (node.hasProperty(LANGUAGES)) {
            values = node.getProperty(LANGUAGES).getValues();
            for (final Value value : values) {
                languages.add(value.getString());
            }
        } else {
            languages = Collections.emptyList();
        }
        Value[] emails;
        List<String> emailList = new ArrayList<>();
        if (node.hasProperty(EMAIL_ADDITIONAL)) {
            emails = node.getProperty(EMAIL_ADDITIONAL).getValues();
            for (final Value value : emails) {
                emailList.add(value.getString());
            }
        } else {
            emailList = Collections.emptyList();
        }
        properties.setProperty(EMAIL_ADDITIONAL, emailList.toString());
        properties.setProperty(LANGUAGES, languages.toString());
        final JCRNodeWrapper nodeWrapper = currentUserSession.getNodeByIdentifier(node.getIdentifier());
        final JahiaUserManagerService jahiaUserManagerService = JahiaUserManagerService.getInstance();
        final JCRNodeWrapper userNode = jahiaUserManagerService.createUser(node.getName(), node
                .getProperty("Password").getString(), properties, currentUserSession);
        nodeWrapper.grantRoles("u:" + userNode.getName(),
                Collections.singleton("owner"));
        node.setProperty("Identity", userNode.getIdentifier());
        final List<String> uuids = Arrays.asList(node.getIdentifier(), userNode.getIdentifier());
        node.refresh(true);
        currentUserSession.save();
        node.getSession().save();
        publish(uuids);
    }

    /**
     * hello
     */
    public static void modifyUser(final PublishedNodeFact publishedNodeFact)
            throws RepositoryException {

        final Node node = publishedNodeFact.getNode();
        final NodeType[] mixinNodeTypes = node.getMixinNodeTypes();
        final List<String> list = new ArrayList<>();
        for (final NodeType nodeType : mixinNodeTypes) {
            list.add(nodeType.getName());
            LOGGER.info(nodeType.getName());
        }
        if (list.contains("jmix:markedForDeletion")) {
            LOGGER.info("Delete user: " + node.getName());
            removeUser(publishedNodeFact);
        } else {
            LOGGER.info("Modifying user: " + node.getName());
            final JCRSessionWrapper currentUserSession = JCRTemplate.getInstance().getSessionFactory()
                    .getCurrentSystemSession("default", null, null);
            final Node jcrUserNode = currentUserSession.getNodeByIdentifier(node.getProperty("Identity").getString());
            LOGGER.info(jcrUserNode.getName() + " " + jcrUserNode.getIdentifier());
            jcrUserNode.setProperty(NAME, node.getProperty(NAME).getString());
            jcrUserNode.setProperty(TITLE, node.getProperty(TITLE).getString());
            if (node.hasProperty(ACADEMIC_TITLE)) {
                jcrUserNode.setProperty(ACADEMIC_TITLE, node.getProperty(ACADEMIC_TITLE).getString());
            } else {
                jcrUserNode.setProperty(ACADEMIC_TITLE, "");
            }
            jcrUserNode.setProperty(SURNAME, node.getProperty(SURNAME).getString());
            jcrUserNode.setProperty(ADDRESS, node.getProperty(ADDRESS).getString());
            jcrUserNode.setProperty(NPA, String.valueOf(node.getProperty(NPA).getLong()));
            jcrUserNode.setProperty(PLACE, node.getProperty(PLACE).getString());
            if (node.hasProperty(PHONE)) {
                jcrUserNode.setProperty(PHONE, node.getProperty(PHONE).getString());
            } else {
                jcrUserNode.setProperty(PHONE, DEF_VAL);
            }
            if (node.hasProperty(CELLPHONE)) {
                jcrUserNode.setProperty(CELLPHONE, node.getProperty(CELLPHONE).getString());
            } else {
                jcrUserNode.setProperty(CELLPHONE, DEF_VAL);
            }
            jcrUserNode.setProperty(EMAIL, node.getProperty(EMAIL).getString());
            if (node.hasProperty(NEWSPAPER)) {
                jcrUserNode.setProperty(NEWSPAPER, node.getProperty(NEWSPAPER).getString());
            } else {
                jcrUserNode.setProperty(NEWSPAPER, DEF_VAL);
            }
            final Value[] values;
            List<String> languages = new ArrayList<>();
            if (node.hasProperty(LANGUAGES)) {
                values = node.getProperty(LANGUAGES).getValues();
                for (final Value value : values) {
                    languages.add(value.getString());
                }
            } else {
                languages = Collections.emptyList();
            }
            jcrUserNode.setProperty(LANGUAGES, languages.toString());
            Value[] emails;
            List<String> emailList = new ArrayList<>();
            if (node.hasProperty(EMAIL_ADDITIONAL)) {
                emails = node.getProperty(EMAIL_ADDITIONAL).getValues();
                for (final Value value : emails) {
                    emailList.add(value.getString());
                }
            } else {
                emailList = Collections.emptyList();
            }

            jcrUserNode.setProperty(EMAIL_ADDITIONAL, emailList.toString());
            jcrUserNode.setProperty("Password", node.getProperty("Password").getString());
            final List<String> uuids = Arrays.asList(node.getIdentifier(), jcrUserNode.getIdentifier());
            jcrUserNode.refresh(true);
            jcrUserNode.getSession().save();
            node.getSession().save();
            currentUserSession.save();
            publish(uuids);
            LOGGER.info("User has been modified successfully!");
        }
    }


    /**
     * hello
     */
    public static void removeUser(final PublishedNodeFact deletedNodeFact)
            throws RepositoryException {
        final String name = deletedNodeFact.getName();
        final JCRSessionWrapper currentUserSession = JCRSessionFactory.getInstance()
                .getCurrentUserSession("default");
        final JahiaUserManagerService jahiaUserManagerService = JahiaUserManagerService.getInstance();
        jahiaUserManagerService.deleteUser(jahiaUserManagerService.getUserPath(name), currentUserSession);
        currentUserSession.save();
    }


    /**
     * hello
     */
    public static void publish(final List<String> uuids)
            throws RepositoryException {
        JCRPublicationService.getInstance().publish(uuids, Constants.EDIT_WORKSPACE,
                Constants.LIVE_WORKSPACE,
                Collections.emptyList());
    }

    private Journalist() {

    }
}
